//
//  HomeCollectionCollectionViewController.swift
//  theque
//
//  Created by William Jacob Ballard on 12/9/19.
//  Copyright © 2019 The Que. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

protocol CollectionCellController {
    
    static func registerCell(on collectionView: UICollectionView)
    func cellFromCollectionView(_ collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> UICollectionViewCell
    func didSelectCell()
    
}


class HomeCollectionViewController: UIViewController, UICollectionViewDataSource {
    

    fileprivate var cellControllers = [CollectionCellController]()
    
    //fileprivate var items = [HomeCellItems]
    
    fileprivate let cellControllerFactory = HomeCellFactory()
    
    fileprivate var items = [CellItems]()
    
    fileprivate var item0 = CellItems.init(isSocial: false, isNav: true)

    @IBOutlet weak var collectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Fuck")
        
        items.append(item0)
        
    
        self.collectionView.dataSource = self
        
        
        cellControllerFactory.registerCells(on: collectionView)
        
        
        cellControllers = cellControllerFactory.cellControllers(with: items)
    }


    // MARK: UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        print("________________________________HEre")
        print("EeeeEeeeEeeeeEeeeeEEeeeeeeeeeeEEeeeeeeeeeEEeeeEeeeEeeeeEeeeeEEeeeeeeeeeeEEeeeeeeeeeEEeeeEeeeEeeeeEeeeeEEeeeeeeeeeeEEeeeeeeeeeEEeeeEeeeEeeeeEeeeeEEeeeeeeeeeeEEeeeeeeeeeE")
        
        return cellControllers[indexPath.row].cellFromCollectionView(collectionView, forIndexPath: indexPath)
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}

class HomeCellFactory {
    
    func registerCells(on collectionView: UICollectionView) {
        
        //register navigation and social feed cells
        NavigationButtonCellController.registerCell(on: collectionView)
    }
    
    func cellControllers(with items: [HomeCellItems]) -> [CollectionCellController] {
        return items.map { item in
            
            if item.isNavigationButtonCellItem {
                return NavigationButtonCellController(item: item)
            } else {
                return NavigationButtonCellController(item: item)
            }
        }
        
    }
    
    
    
    
}

protocol HomeCellItems {
    
    var isNavigationButtonCellItem : Bool{get}
    
    var isSocialFeedItem : Bool{get}
    
    
}

struct CellItems : HomeCellItems {
    var isSocialFeedItem: Bool
    
    var isNavigationButtonCellItem: Bool
    
    init(isSocial : Bool, isNav : Bool) {
        self.isSocialFeedItem = isSocial
        self.isNavigationButtonCellItem = isNav
    }
}



//Top navigation controller for both registering xib to collection and modifying it for presentation
class NavigationButtonCellController : CollectionCellController {

    fileprivate let item : HomeCellItems

    init(item: HomeCellItems) {
        self.item = item
    }
    
    fileprivate static var cellIdentifier: String {
        return String(describing: type(of: MusicNavigationButtonView.self))
    }
    
    static func registerCell(on collectionView: UICollectionView) {

        let xib = UINib.init(nibName:"MusicNavigationButtonView", bundle: nil)
        
        collectionView.register(xib, forCellWithReuseIdentifier:"MusicNavigationButtonView")
        
        
        
    }

    func cellFromCollectionView(_ collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : MusicNavigationButtonView = collectionView.dequeueReusableCell(withReuseIdentifier:"MusicNavigationButtonView", for: indexPath) as! MusicNavigationButtonView
        
        //Do modification here
        
        cell.backgroundColor = UIColor.systemRed
        
        return cell
        
    }

    func didSelectCell() {
        print("Selected NavigationButtonCell")
    }

}



class MusicNavigationButtonView : UICollectionViewCell {
    
    @IBOutlet weak var songsButton: UIButton!
    
    @IBOutlet weak var favoritesButton: UIButton!
    
    @IBOutlet weak var playlistsButton: UIButton!
    
    @IBAction func songsButtonPressed(_ sender: Any) {
        
        print("Songs button pressed")
        
    }
    
    @IBAction func favoritesButtonPressed(_ sender: Any) {
        
        print("Favorites button pressed")
        
    }
    
    @IBAction func playlistsButtonPressed(_ sender: Any) {
        
        print("Playlists button pressed")
        
    }
    
}



